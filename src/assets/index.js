import music from "./images/projects/music.png";
import kaisCarts from "./images/projects/kaisCarts.png";
import taskManagement from "./images/projects/taskManagement.png";
import contactImg from "./images/contact/contactImg.png";
import ProfilePic from "./images/ProfilePic.png";
import wardrobify from "./images/projects/wardrobify.png"

export {
  music,
  kaisCarts,
  taskManagement,
  contactImg,
  ProfilePic,
  wardrobify,
};
