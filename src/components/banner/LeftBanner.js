import React from 'react'
import { useTypewriter, Cursor } from "react-simple-typewriter";
import { FaLinkedinIn, FaGitlab } from "react-icons/fa";

const LeftBanner = () => {
    const [text] = useTypewriter({
      words: ["Software Engineer.", "Full Stack Developer."],
      loop: true,
      typeSpeed: 20,
      deleteSpeed: 10,
      delaySpeed: 2000,
    });
  return (
    <div className="w-full lgl:w-1/2 flex flex-col gap-20">
      <div className="flex flex-col gap-5">
        <h1 className="text-6xl font-bold text-white">
          Hi, I'm <span className="text-designColor capitalize">Daniel Berk</span>
        </h1>
        <h2 className="text-4xl font-bold text-white">
          <span>{text}</span>
          <Cursor
            cursorBlinking="false"
            cursorStyle="|"
            cursorColor="#2bff01"
          />
        </h2>
        <p className="text-base font-bodyFont leading-6 tracking-wide">
          Welcome to my portfolio page. I am a recent graduate of Hack Reactor's Software Engineering bootcamp. My projects include both student projects and personal projects.
        </p>
      </div>
      <div className="flex flex-col xl:flex-row gap-6 lgl:gap-0 justify-between">
        <div>
          <h2 className="text-base uppercase font-titleFont mb-4">
            Find me on
          </h2>
          <div className="flex gap-4">
            <a href='https://gitlab.com/DanBerk'>
              <span className="bannerIcon">
                <FaGitlab />
              </span>
            </a>
            <a href='https://www.linkedin.com/in/danieljberk/'>
              <span className="bannerIcon">
                <FaLinkedinIn />
              </span>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LeftBanner
