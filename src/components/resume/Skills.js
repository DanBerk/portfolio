import React from 'react'
import { motion } from 'framer-motion';

const Skills = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1, transition: { duration: 0.5 } }}
      className="w-full flex flex-col lgl:flex-row gap-10 lgl:gap-20"
    >
      <div className="w-full lgl:w-1/4">
        <div className="py-12 font-titleFont flex flex-col gap-4">
          <h2 className="text-3xl md:text-4xl font-bold">Programming Languages</h2>
        </div>
        <div className="flex flex-col gap-6">
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
              <iconify-icon style={{marginRight: 10}} width="25" icon="logos:python"></iconify-icon>
              Python 3
            </p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
              <iconify-icon style={{marginRight: 10}} width="25" icon="logos:javascript"></iconify-icon>
              JavaScript ES6+
            </p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
              <iconify-icon style={{marginRight: 10}} width="25" icon="logos:html-5"></iconify-icon>
              HTML5
            </p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
              <iconify-icon style={{marginRight: 10}} width="25" icon="logos:css-3"></iconify-icon>
              CSS
            </p>
          </div>
        </div>
      </div>
      <div className="w-full lgl:w-1/4">
        <div className="py-12 font-titleFont flex flex-col gap-4">
          <h2 className="text-3xl md:text-4xl font-bold">Front-End</h2>
        </div>
        <div className="flex flex-col gap-6">
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
            <iconify-icon style={{marginRight: 10}} width="25" icon="logos:react"></iconify-icon>
              React
            </p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">React Hooks</p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">DOM manipulation</p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">WebSockets</p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
              <iconify-icon style={{marginRight: 10}} icon="logos:redux" width="25"></iconify-icon>
              Redux Toolkit
            </p>
          </div>
        </div>
      </div>
      <div className="w-full lgl:w-1/4">
        <div className="py-12 font-titleFont flex flex-col gap-4">
          <h2 className="text-3xl md:text-4xl font-bold">Back-End</h2>
        </div>
        <div className="flex flex-col gap-6">
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
              <iconify-icon style={{marginRight: 10}} icon="logos:django-icon" width="25"></iconify-icon>
              Django 4
            </p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
              <iconify-icon style={{marginRight: 10}} icon="logos:postgresql" width="25"></iconify-icon>
              PostgreSQL
            </p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
              <iconify-icon style={{marginRight: 10}} icon="skill-icons:mongodb" width="25"></iconify-icon>
              MongoDB
            </p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
              <iconify-icon style={{marginRight: 10}} icon="skill-icons:fastapi" width="25"></iconify-icon>
              FastAPI
            </p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
              <iconify-icon style={{marginRight: 10}} icon="logos:rabbitmq-icon" width="25"></iconify-icon>
              RabbitMQ
            </p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">
              <iconify-icon style={{marginRight: 10}} icon="logos:docker-icon" width="25"></iconify-icon>
              Docker
            </p>
          </div>
        </div>
      </div>
      <div className="w-full lgl:w-1/4">
        <div className="py-12 font-titleFont flex flex-col gap-4">
          <h2 className="text-3xl md:text-4xl font-bold">System Design</h2>
        </div>
        <div className="flex flex-col gap-6">
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">Monoliths</p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">Microservices</p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">Domain-driven design</p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">Message passing</p>
          </div>
          <div className="overflow-x-hidden">
            <p className="text-sm uppercase font-medium">Event Sourcing</p>
          </div>
        </div>
      </div>
    </motion.div>
  );
}

export default Skills
