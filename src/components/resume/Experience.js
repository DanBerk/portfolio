import React from "react";
import {motion} from "framer-motion"
import ResumeCard from "./ResumeCard";

const Experience = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1, transition: { duration: 0.5 } }}
      className="py-12 font-titleFont flex gap-20"
    >
      <div>
        <div className="flex flex-col gap-4">
          <p className="text-sm text-designColor tracking-[4px]">2014-2023</p>
          <h2 className="text-4xl font-bold">Job Experience</h2>
        </div>
        <div className="mt-14 w-full border-l-[6px] border-l-black border-opacity-30 flex flex-col gap-10">
          <ResumeCard
            title="Production and IT Manager"
            subTitle="CKC Graphics - (2016 - 2023)"
            des="Performed all IT setup and maintenance of servers, computers, large format printers, cutters, and laminators leading to a 25% improvement in production output. Executed extremely detailed preparation and installation work of vehicle graphics and signs. Set up and trained on new equipment leading to a substantial increase in revenue opportunities.
            "
          />
          <ResumeCard
            title="Crew Member"
            subTitle="Dairy Queen - (2014 - 2016)"
          />
        </div>
      </div>
    </motion.div>
  );
};

export default Experience;
