import React from 'react'
import Title from '../layouts/Title'
import { music, kaisCarts, taskManagement, wardrobify } from "../../assets/index";
import ProjectsCard from './ProjectsCard';

const Projects = () => {
  return (
    <section
      id="projects"
      className="w-full py-20 border-b-[1px] border-b-black"
    >
      <div className="flex justify-center items-center text-center">
        <Title
          des="My Projects"
        />
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-6 xl:gap-14">
        <ProjectsCard
          title="TuneWorld"
          des="A fully deployed web application for searching for artists, albums, and songs"
          src={music}
          gitlab="https://gitlab.com/team-adds/tuneworld"
          link="https://team-adds.gitlab.io/tuneworld"
        />
        <ProjectsCard
          title="Kai's Carts"
          des="A full-stack web application keeping track of vehicle inventory, sales, and service appointments"
          src={kaisCarts}
          gitlab="https://gitlab.com/DanBerk/kais-carts"
        />
        <ProjectsCard
          title="Task Management"
          des="A web application used for adding and keeping track of tasks and projects"
          src={taskManagement}
          gitlab="https://gitlab.com/DanBerk/task-management"
        />
        <ProjectsCard
          title="Wardrobify"
          des="An application for keeping track of your shoes and hats"
          src={wardrobify}
          gitlab="https://gitlab.com/DanBerk/Wardrobify"
        />
      </div>
    </section>
  );
}

export default Projects
